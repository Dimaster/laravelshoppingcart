<?php

namespace Web24\Shoppingcart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException {}
