<?php

namespace Web24\Shoppingcart\Exceptions;

use RuntimeException;

class UnknownModelException extends RuntimeException {}
